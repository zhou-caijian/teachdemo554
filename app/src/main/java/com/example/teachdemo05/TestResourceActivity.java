package com.example.teachdemo05;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class TestResourceActivity extends AppCompatActivity {
    TextView tv_msg;
    LinearLayout ll_layout;
    ImageView iv_alpha,iv_frame;
    Animation alphaAnimation;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_resource);

        tv_msg = findViewById(R.id.tv_msg);
        ll_layout = findViewById(R.id.ll_layout);
        iv_alpha = findViewById(R.id.iv_alpha);
        iv_frame = findViewById(R.id.iv_frame);

        tv_msg.setText(R.string.test_title2);
        tv_msg.setTextColor(getResources().getColor(R.color.colorAccent));

        String[] strArray = getResources().getStringArray(R.array.str_array) ;

        String strText = "";

        for(String str : strArray){
            strText += "  " + str;
        }

        tv_msg.setText(strText);

        LayoutInflater inflater = LayoutInflater.from(this);
        inflater.inflate(R.layout.my_textview,ll_layout);

        //给组件注册上下文菜单

        registerForContextMenu(tv_msg);
        //装载动画
        alphaAnimation = AnimationUtils.loadAnimation(this,R.anim.alpha_animation);
    }
    //重写选项菜单方法（onCreateOptionsMenu方法）
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //添加菜单项
        menu.add(1,1,0,"new");
        menu.add(1,2,0,"open");
        menu.add(1,3,0,"save");
        menu.add(1,4,0,"delete");


        return super.onCreateOptionsMenu(menu);
    }
    //添加选项菜单项的选中事件(重写onOptionsItemSelected方法)

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        return super.onOptionsItemSelected(item);
    }

    //重写上下文菜单方法（onCreateContextMenu）

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        //super.onCreateContextMenu(menu, v, menuInfo);

        MenuInflater inflater = new MenuInflater(this);
        inflater.inflate(R.menu.my_menu,menu);
    }

    //添加上下文菜单项的选中事件(重写onContextItemSelected方法)
    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {

        if(item.getItemId() == R.id.menu_add){
            Toast.makeText(this, "menu_add", Toast.LENGTH_SHORT).show();
        }
        if(item.getItemId() == R.id.menu_delete){
            Toast.makeText(this, "menu_delete", Toast.LENGTH_SHORT).show();
        }
        if(item.getItemId() == R.id.menu_update){
            Toast.makeText(this, "menu_update", Toast.LENGTH_SHORT).show();
        }
        if(item.getItemId() == R.id.menu_query){
            Toast.makeText(this, "menu_query", Toast.LENGTH_SHORT).show();
        }
        return super.onContextItemSelected(item);
    }

    public void alphaStart(View view) {

        //AlphaAnimation animation = new AlphaAnimation(0.0f,1.0f);
        //animation.setDuration(5000);
        //启动动画
        iv_alpha.startAnimation(alphaAnimation);
    }

    public void frameStart(View view) {

        iv_frame.setImageResource(R.drawable.frame_animation);
        AnimationDrawable drawable = (AnimationDrawable)iv_frame.getDrawable();
        drawable.start();

    }
}







































