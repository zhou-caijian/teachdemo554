package com.example.teachdemo05;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.teachdemo05.model.Student;
import com.example.teachdemo05.model.User;

import java.util.ArrayList;
import java.util.List;

public class ForResultActivity extends AppCompatActivity {

    Button btn_get;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_for_result);

        btn_get = findViewById(R.id.btn_get);

        btn_get.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ForResultActivity.this,ResultActivity.class);  //创建Intent对象，指定源界面--Activity，目标界面--Activity

                String strValue = "Android";
                int intValue = 1010;

                Bundle bundle = new Bundle();
                bundle.putString("str_key",strValue);
                bundle.putInt("int_key",intValue);

                intent.putExtras(bundle);

                startActivityForResult(intent,0x010);

            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 0x010 && resultCode == 0x011){
            if(data != null){
                Log.v("data",data.getStringExtra("re_name"));
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

}
