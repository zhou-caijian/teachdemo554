package com.example.teachdemo05;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;

public class GalleryActivity extends AppCompatActivity {

    Gallery gallery;

    int[] imgIds = new int[]{
            R.mipmap.img01,R.mipmap.img02,R.mipmap.img03,
            R.mipmap.img04,R.mipmap.img05,R.mipmap.img06
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);

        gallery = findViewById(R.id.gallery);

        BaseAdapter baseAdapter = new BaseAdapter() {
            @Override
            public int getCount() {
                return imgIds.length;
            }

            @Override
            public Object getItem(int position) {
                return imgIds[position];
            }

            @Override
            public long getItemId(int position) {
                return position;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup viewGroup) {

                ImageView imageView;
                if(convertView == null){
                    imageView = new ImageView(GalleryActivity.this);
                    imageView.setPadding(20,20,50,20);
                }else{
                    imageView = (ImageView) convertView;
                }


                imageView.setImageResource(imgIds[position]);

                return imageView;
            }
        };

        gallery.setAdapter(baseAdapter);

        gallery.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Log.v("click",""+i);
            }
        });
        gallery.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {

                Log.v("long",""+i);
                return false;
            }
        });
        gallery.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Log.v("selected",""+i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Log.v("no","");
            }
        });

    }
}
