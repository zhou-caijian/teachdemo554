package com.example.teachdemo05;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.teachdemo05.model.User;

import java.util.Random;


public class ProgressBarActivity extends AppCompatActivity {

    ProgressBar progressBar;
    Button btnSet,btnStop;
    TextView tvMsg;
    int value = 5;
    boolean isTop = false;
    Handler handler;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_progress_bar);

        progressBar = findViewById(R.id.progressBar);
        btnSet = findViewById(R.id.btnSet);
        btnStop = findViewById(R.id.btnStop);
        tvMsg = findViewById(R.id.tvMsg);

        progressBar.setProgress(value);

        handler = new Handler(){
            @Override
            public void handleMessage(@NonNull Message msg) {
                super.handleMessage(msg);

                if(msg.what == 0x110){
                    User user = (User) msg.obj;
                    progressBar.setProgress(msg.arg1);
                    tvMsg.setText("value("+ user.getUid() +"):"+msg.arg1);
                    //Log.v("msg","0x110");
                }
            }
        };

        //使用AlertDialog显示信息提示，设置提示内容，如果有按钮添加按钮事件

        btnSet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        while (!isTop){
                            Random random = new Random();

                            int intValue = random.nextInt(10);

                            value += intValue;

                            Log.v("random:" , ""+intValue + " value:" + value);
                            /*
                            if(value >= 100){
                                progressBar.setProgress(100);
                                tvMsg.setText("value:"+100);

                                break;
                            }*/
                            if(value >= 100){
                                value = 100;
                                isTop = true;
                            }
                            //progressBar.setProgress(value);//老版本会报错
                            //tvMsg.setText("value:"+value);//老版本会报错
                            if(handler != null){
                                User user = new User("1001","zhang");
                                //handler.sendEmptyMessage(0x110);
                                Message message = new Message();
                                message.what = 0x110;
                                message.arg1 = value;
                                message.obj = user;
                                handler.sendMessage(message);
                            }
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }).start();



            }
        });
        btnStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                isTop = true;
            }
        });

    }
}
