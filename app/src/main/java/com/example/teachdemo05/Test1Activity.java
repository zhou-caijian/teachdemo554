package com.example.teachdemo05;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.teachdemo05.model.Student;
import com.example.teachdemo05.model.User;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Test1Activity extends AppCompatActivity {

    Button btn_go;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test1);

        btn_go = findViewById(R.id.btn_go);

        btn_go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Test1Activity.this,Test2Activity.class);  //创建Intent对象，指定源界面--Activity，目标界面--Activity

                String strValue = "Android";
                int intValue = 1010;
                String[] strArray = new String[]{"111","222","333"};
                List<String> list = new ArrayList<>();
                list.add("aaa");
                list.add("bbb");
                list.add("ccc");

                User user = new User("10001","zhangsan");
                Student user1 = new Student("10002","zhangsan");
                Student user2 = new Student("10003","lisi");
                Student user3 = new Student("10004","wangwu");
                ArrayList<Student> studentList = new ArrayList<>();
                studentList.add(user1);
                studentList.add(user2);
                studentList.add(user3);



                Bundle bundle = new Bundle();
                bundle.putString("str_key",strValue);
                bundle.putInt("int_key",intValue);
                bundle.putStringArray("array_key",strArray);
                bundle.putStringArrayList("list_key", (ArrayList<String>) list);
                bundle.putSerializable("user_key", user);

                bundle.putParcelableArrayList("par_key",studentList);
                intent.putExtras(bundle);

                startActivity(intent);//启动目标Activity

                finish();//关闭当前Activity
            }
        });
    }
}
