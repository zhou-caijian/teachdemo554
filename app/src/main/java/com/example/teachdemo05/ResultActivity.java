package com.example.teachdemo05;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.teachdemo05.model.Student;
import com.example.teachdemo05.model.User;

import java.util.ArrayList;

public class ResultActivity extends AppCompatActivity {
    TextView tv_msg;
    Button btn_back;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_result);

        tv_msg = findViewById(R.id.tv_msg);
        btn_back = findViewById(R.id.btn_back);

        Intent intent = getIntent();

        Bundle bundle = intent.getExtras();

        tv_msg.setText(bundle.getString("str_key","哈哈，你拿不到我") + "  " + bundle.getInt("int_key",0));

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent();
                intent.putExtra("re_name","返回来的结果");
                setResult(0x011,intent);

                finish();
            }
        });
    }
}
