package com.example.teachdemo05.model;

import android.os.Parcel;
import android.os.Parcelable;


public class Student implements Parcelable {

    private String uid;
    private String uname;

    public Student(){}

    public Student(String uid) {
        this.uid = uid;
    }

    public Student(String uid, String uname) {
        this.uid = uid;
        this.uname = uname;
    }


    protected Student(Parcel in) {
        uid = in.readString();
        uname = in.readString();
    }

    public static final Creator<Student> CREATOR = new Creator<Student>() {
        @Override
        public Student createFromParcel(Parcel in) {
            return new Student(in);
        }

        @Override
        public Student[] newArray(int size) {
            return new Student[size];
        }
    };
    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getUname() {
        return uname;
    }

    public void setUname(String uname) {
        this.uname = uname;
    }

    @Override
    public String toString() {
        return "学生{" +
                "用户ID='" + uid + '\'' +
                ", 用户名='" + uname + '\'' +
                '}';
    }
    @Override
    public int describeContents() {
        return 0;
    }
    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(uid);
        parcel.writeString(uname);
    }
}
