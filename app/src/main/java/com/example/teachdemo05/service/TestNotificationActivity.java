package com.example.teachdemo05.service;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.JobIntentService;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.teachdemo05.R;

public class TestNotificationActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_notification);
    }

    public void startService(View view) {
        Intent intent = new Intent();
        intent.putExtra("str_key","传递字符串参数3");
        JobIntentService.enqueueWork(this,MyJobIntentService.class,0x110,intent);
    }
}
