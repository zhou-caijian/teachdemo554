package com.example.teachdemo05.service;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.JobIntentService;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.example.teachdemo05.R;

public class TestMyIntentServiceActivity extends AppCompatActivity {

    TextView tv_msg;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_my_intent_service);
        tv_msg = findViewById(R.id.tv_msg);
    }

    public void startServcie(View view) {
        Log.v("msg","点击了按钮");
        Intent intent = new Intent(TestMyIntentServiceActivity.this,MyIntentService.class);
        intent.putExtra("str_key","传递字符串参数");

        startService(intent);
    }

    public void startJobService(View view) {

        Intent intent = new Intent();
        intent.putExtra("str_key","传递字符串参数2");
        JobIntentService.enqueueWork(this,MyJobIntentService.class,0x110,intent);
    }

    ResultReceiver receiver = new ResultReceiver(new Handler()){
        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            super.onReceiveResult(resultCode, resultData);
            if(resultCode == 0x112){
                tv_msg.setText(resultData.getString("send_key","no"));
            }
        }
    };

    public void resultReceiver(View view) {

        Intent intent = new Intent();
        intent.putExtra("str_key","传递字符串参数3");
        intent.putExtra("receiver_key",receiver);

        JobIntentService.enqueueWork(this,MyJobIntentService.class,0x111,intent);
    }
}
