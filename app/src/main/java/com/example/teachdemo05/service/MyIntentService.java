package com.example.teachdemo05.service;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;

public class MyIntentService extends IntentService {
    /**
     * @param name
     * @deprecated
     */
    public MyIntentService(String name) {
        super(name);
    }
    public MyIntentService() {
        super("myservcie");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {//启动service时自动执行此方法


        Log.v("service:","执行onHandleIntent");

        if(intent != null){
            Log.v("intent数据:",intent.getStringExtra("str_key"));
            //Toast.makeText(this, intent.getStringExtra("str_key"), Toast.LENGTH_SHORT).show();
        }
    }
}
