package com.example.teachdemo05.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.JobIntentService;
import androidx.core.app.NotificationCompat;

import com.example.teachdemo05.R;

public class MyJobIntentService extends JobIntentService {

    public static final String ACTION = "com.example.teachdemo05.XXX";
    @Override
    protected void onHandleWork(@NonNull Intent intent) {//服务启动时会执行此方法

//        Log.v("job","onHandleWork");
//        if(intent != null){
//            Log.v("str_key",intent.getStringExtra("str_key"));
//        }

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        /*
        Log.v("job","onHandleWork");
        if(intent != null){
            Log.v("str_key",intent.getStringExtra("str_key"));

            ResultReceiver receiver = (ResultReceiver) intent.getExtras().get("receiver_key");
            Bundle bundle = new Bundle();
            bundle.putString("send_key","send_value");

            receiver.send(0x112,bundle);
        }
        */
        /*
        Intent sendIntent = new Intent(ACTION);
        sendIntent.putExtra("send_key","send_value");
        sendBroadcast(sendIntent);
         */

        /*
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this,"11");
        builder.setContentTitle("通知标题")
        .setContentText("通知内容")
        .setContentInfo("info")
        .setSmallIcon(R.mipmap.ic_launcher)
        .setLargeIcon(BitmapFactory.decodeResource(getResources(),R.mipmap.ic_launcher));

        Notification notification = builder.build();

        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        manager.notify(1,notification);

         */
        if(Build.VERSION.SDK_INT >= 26){
            String channelId = "channel1";
            String name = "0x11";
            int importance = NotificationManager.IMPORTANCE_LOW;
            NotificationChannel channel = new NotificationChannel(channelId,name,importance);

            NotificationCompat.Builder builder = new NotificationCompat.Builder(this,channelId);
            builder.setContentTitle("通知标题")
                    .setContentText("通知内容")
                    .setContentInfo("info")
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setLargeIcon(BitmapFactory.decodeResource(getResources(),R.mipmap.ic_launcher));

            Notification notification = builder.build();

            NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            manager.createNotificationChannel(channel);
            manager.notify(1,notification);

        }else{

            NotificationCompat.Builder builder = new NotificationCompat.Builder(this,"11");
            builder.setContentTitle("通知标题")
                    .setContentText("通知内容")
                    .setContentInfo("info")

                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setLargeIcon(BitmapFactory.decodeResource(getResources(),R.mipmap.ic_launcher));

            Notification notification = builder.build();

            NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            manager.notify(1,notification);
        }

        //扩展：1、自定义通知样式；2、通知跳转到其他页面；3、文件下载（以通知的形式展示，打开）




    }
}
