package com.example.teachdemo05.service;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.JobIntentService;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.example.teachdemo05.R;

public class TestBroadcastActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_broadcast);

    }

    public void startServcie(View view) {
        Intent intent = new Intent();
        intent.putExtra("str_key","传递字符串参数3");
        JobIntentService.enqueueWork(this,MyJobIntentService.class,0x110,intent);
        startActivity(new Intent(this,TestBroadcast2Activity.class));

    }

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.v("receive",intent.getStringExtra("send_key"));
        }
    };


    @Override
    protected void onResume() {
        super.onResume();

        IntentFilter filter = new IntentFilter(MyJobIntentService.ACTION);

        registerReceiver(broadcastReceiver,filter);
    }

    @Override
    protected void onPause() {
        super.onPause();

        unregisterReceiver(broadcastReceiver);
    }
}
