package com.example.teachdemo05;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class RecyclerViewActivity extends AppCompatActivity {

    RecyclerView rv_contact;
    int[] imgIds = new int[]{
            R.mipmap.img01,R.mipmap.img02,R.mipmap.img03,
            R.mipmap.img04,R.mipmap.img05,R.mipmap.img06,
            R.mipmap.img01,R.mipmap.img02,R.mipmap.img03,
            R.mipmap.img04,R.mipmap.img05,R.mipmap.img06,
            R.mipmap.img01,R.mipmap.img02,R.mipmap.img03,
            R.mipmap.img04,R.mipmap.img05,R.mipmap.img06,
            R.mipmap.img01,R.mipmap.img02,R.mipmap.img03,
            R.mipmap.img04,R.mipmap.img05,R.mipmap.img06,
            R.mipmap.img01,R.mipmap.img02,R.mipmap.img03,
            R.mipmap.img04,R.mipmap.img05,R.mipmap.img06,
            R.mipmap.img01,R.mipmap.img02,R.mipmap.img03,
            R.mipmap.img04,R.mipmap.img05,R.mipmap.img06
    };
    String[] strNames = new String[]{
            "1111","2222","33333","44444","5555","66666",
            "1111","2222","33333","44444","5555","66666",
            "1111","2222","33333","44444","5555","66666",
            "1111","2222","33333","44444","5555","66666",
            "1111","2222","33333","44444","5555","66666",
            "1111","2222","33333","44444","5555","66666"

    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_view);

        rv_contact = findViewById(R.id.rv_contact);

        ContactAdapter adapter = new ContactAdapter();

        rv_contact.setAdapter(adapter);
        rv_contact.setLayoutManager(new LinearLayoutManager(RecyclerViewActivity.this));
    }

    class ContactViewHolder extends RecyclerView.ViewHolder{
        ImageView iv_contact;
        TextView tv_contact;

        public ContactViewHolder(@NonNull View itemView) {
            super(itemView);

            iv_contact = itemView.findViewById(R.id.iv_contact);
            tv_contact = itemView.findViewById(R.id.tv_contact);
        }
    }

    class ContactAdapter extends RecyclerView.Adapter<ContactViewHolder>{

        @NonNull
        @Override
        public ContactViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

            ContactViewHolder holder = new ContactViewHolder(LayoutInflater.from(RecyclerViewActivity.this).inflate(R.layout.rv_item,parent,false));

            return holder;
        }

        @Override
        public void onBindViewHolder(@NonNull ContactViewHolder holder, int position) {

            holder.iv_contact.setImageResource(imgIds[position]);
            holder.tv_contact.setText(strNames[position]);

        }

        @Override
        public int getItemCount() {
            return strNames.length;
        }
    }
}
