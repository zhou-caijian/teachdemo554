package com.example.teachdemo05;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class TestEventActivity extends AppCompatActivity implements  View.OnClickListener {
    Button btn_event1,btn_event2,btn_event3,btn_event4;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_event);

        btn_event1 = findViewById(R.id.btn_event1);
        btn_event2 = findViewById(R.id.btn_event2);
        btn_event3 = findViewById(R.id.btn_event3);
        btn_event4 = findViewById(R.id.btn_event4);
        //第一种方式
        btn_event1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        btn_event2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        btn_event3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        btn_event4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        //第三种方式--给按钮设定监听

        btn_event1.setOnClickListener(this);
        btn_event2.setOnClickListener(this);
        btn_event3.setOnClickListener(this);
        btn_event4.setOnClickListener(this);
    }
    //第二种方式--布局文件里面的onClick属性
    public void clickEvent2(View view) {

        if(view.getId()  == R.id.btn_event2){

        }
        if(view.getId()  == R.id.btn_event3){

        }
        if(view.getId()  == R.id.btn_event4){

        }
    }
    //第三种方式--让当前的类实现View.OnClickListener接口
    @Override
    public void onClick(View view) {
        if(view.getId()  == R.id.btn_event2){

        }
        if(view.getId()  == R.id.btn_event3){

        }
        if(view.getId()  == R.id.btn_event4){

        }
    }
}
interface OnButtonClickListener extends View.OnClickListener{//自定义接口
    void onMyClick(View view);
}
























